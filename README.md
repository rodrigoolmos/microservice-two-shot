# Wardrobify

Team:

* Brandon Souvannarath - Shoes
* Rodrigo Olmos - Hats

## Design

## Shoes microservice

The shoe micrioservice utilizes a value object model for the bin in order to reference the bin without changing the database.

The microservice also has a model named shoe, within that model contains a foreign key that points to the value object model.
This creates a one to many relationship that organizes shoes to one bin and one bin only.

Within the poller, the databases are updated by polling data from the wardrobe microservice.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
