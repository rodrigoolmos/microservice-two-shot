from .models import Shoe, BinVO

from common.json import ModelEncoder



class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_size",
        "bin_number",
        "import_href",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "pic_url",
        "bin",
    ]
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "pic_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder()
    }
