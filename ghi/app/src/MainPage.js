import { Link } from 'react-router-dom';
import React, { useState, useEffect } from 'react'

function MainPage() {
  const [shoes, setShoes] = useState([])

  const getShoes = async () => {
    const shoeUrl = 'http://localhost:8080/api/bins'
    const shoeResponse = await fetch(shoeUrl);

    if (shoeResponse.ok) {
      const data = await shoeResponse.json();
      const shoes = data.shoes
      setShoes(shoes)
    }
  }

  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
      </div>
    </div>
  );
}

export default MainPage;
