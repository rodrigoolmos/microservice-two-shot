import React, { useState, useEffect } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import BinsList from './BinsList';
import BinForm from './BinForm';

function App() {
  const [shoes, setShoes] = useState([])
  const [bins, setBins] = useState([])

  const getShoes = async () => {
    const shoeUrl = 'http://localhost:8080/api/bins'
    const shoeResponse = await fetch(shoeUrl);

    if (shoeResponse.ok) {
      const data = await shoeResponse.json();
      const shoes = data.shoes
      setShoes(shoes)
    }
  }
  const getBins = async () => {
    const binUrl = 'http://localhost:8100/api/bins'
    const binResponse = await fetch(binUrl);

    if (binResponse.ok) {
      const data = await binResponse.json();
      const bins = data.bins
      setBins(bins)
    }
  }

  useEffect( () => {
    getShoes();
    getBins();
  }, [


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
